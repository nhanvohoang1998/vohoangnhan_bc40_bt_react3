import React, { Component } from 'react'
import ItemShoe from './ItemShoe'

export default class ListShoe extends Component {
    handleRenderShoe = () => {
        return this.props.dataShoe.map((item) => {
            return (
                <ItemShoe handleAddToCart={this.props.handleAddToCart} dataShoe={item} />
            )
        })
    }
    render() {
        console.log(this.props.dataShoe)
        return (
           
                <div className='row justify-content-around'>
                    <this.handleRenderShoe />
                </div>
        

        )
    }
}
