import React, { Component } from 'react'
import Cart from './Cart'
import { dataShoe } from './data_shoe'
import ListShoe from './ListShoe'

export default class ShoeShop extends Component {
  state = {
    dataShoe: dataShoe,
    dataCart: [],
  }

  handleAddToCart = (shoe)=>{
    let cloneCart = [...this.state.dataCart]
    let index = cloneCart.findIndex((item)=>{
      return shoe.id === item.id
    })
    if(index === -1){
      let newShoe = {...shoe,soLuong:1}
      cloneCart.push(newShoe)
    }else{
      cloneCart[index].soLuong+=1;
    }
    this.setState({dataCart:cloneCart})
  }

  handleRemoveItem =(item)=>{
    let cloneCart = [...this.state.dataCart]
    let index = cloneCart.findIndex((shoe)=>{
      return item.id === shoe.id
    })
    cloneCart.splice(index,1)
    this.setState({dataCart:cloneCart})
  }

  handleChangeQuantity = (check,shoe) => { 
    let cloneCart = [...this.state.dataCart]
    let index = cloneCart.findIndex((item)=>{
      return item.id === shoe.id
    })
    if(check){
      cloneCart[index].soLuong+=1;
    }else{
      cloneCart[index].soLuong-=1;
      if(cloneCart[index].soLuong==0){
        cloneCart.splice(index,1)
      }
    }
    this.setState({dataCart:cloneCart})
   }

  render() {
    return (
      <div className='container'>
        <h1>List Shoe</h1>
        <Cart handleChangeQuantity={this.handleChangeQuantity} handleRemoveItem={this.handleRemoveItem} dataCart={this.state.dataCart} />
        <ListShoe handleAddToCart={this.handleAddToCart} dataShoe={this.state.dataShoe} />
      </div>
    )
  }
}
