import React, { Component } from 'react'

export default class Cart extends Component {
    handleRenderCart = () => {
        return this.props.dataCart.map((item) => {
            let { id, name, soLuong, price, image } = item
            return (
                <tr>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>
                        <button onClick={()=>{this.props.handleChangeQuantity(false,item)}} className='btn btn-danger'>-</button>
                        <strong className='mx-3'>{soLuong}</strong>
                        <button onClick={()=>{this.props.handleChangeQuantity(true,item)}} className='btn btn-success'>+</button>
                    </td>
                    <td>{price * soLuong}</td>
                    <td><img src={image} style={{ width: 50 }} alt="" /></td>
                    <td>
                        <button onClick={()=>{this.props.handleRemoveItem(item)}} className='btn btn-warning'>Xoa</button>
                    </td>
                </tr>
            )
        })
    }
    render() {
        return (
            <div>
                <table className='table text-center'>
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Img</th>
                        <th>Option</th>
                    </thead>
                    <tbody>
                        {this.handleRenderCart()}
                    </tbody>
                </table>
            </div>
        )
    }
}
