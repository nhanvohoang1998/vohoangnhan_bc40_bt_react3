import React, { Component } from 'react'

export default class ItemShoe extends Component {
    render() {
        let {name, price, description, image}=this.props.dataShoe
        return (
            <div className='col-4 mb-3'>
                <div class="card border-primary">
                    <img class="card-img-top" src={image} alt="" />
                    <div style={{height:200}} class="card-body">
                        <h4 class="card-title">{name}</h4>
                        <p class="card-text">{price}</p>
                        <button onClick={()=>{this.props.handleAddToCart(this.props.dataShoe)}} className='btn btn-warning'>Add to Cart</button>
                    </div>
                </div>

            </div>
        )
    }
}
